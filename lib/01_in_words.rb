class Fixnum

  def in_words
    ones = {
      0 => "zero",
      1 => "one",
      2 => "two",
      3 => "three",
      4 => "four",
      5 => "five",
      6 => "six",
      7 => "seven",
      8 => "eight",
      9 => "nine"
    }

    teens = {
      10 => "ten",
      11 => "eleven",
      12 => "twelve",
      13 => "thirteen",
      14 => "fourteen",
      15 => "fifteen",
      16 => "sixteen",
      17 => "seventeen",
      18 => "eighteen",
      19 => "nineteen"
    }

    tens = {
      20 => "twenty",
      30 => "thirty",
      40 => "forty",
      50 => "fifty",
      60 => "sixty",
      70 => "seventy",
      80 => "eighty",
      90 => "ninety"
    }

    places = {
      100 => "hundred",
      1_000 => "thousand",
      1_000_000 => "million",
      1_000_000_000 => "billion",
      1_000_000_000_000 => "trillion"
    }

    if self < 10
      ones[self]
    elsif self < 20
      teens[self]
    elsif self < 100
      if self % 10 == 0
        tens[(self / 10) * 10]
      else
        tens[(self / 10) * 10] + " " + (self % 10).in_words
      end
    else
      magnitude = find_magnitude
      if self % magnitude == 0
        (self / magnitude).in_words + " " + places[magnitude]
      else
        (self / magnitude).in_words + " " + places[magnitude] + " " +
        (self % magnitude).in_words
      end
    end
  end

  def find_magnitude
    mags = [100, 1_000, 1_000_000, 1_000_000_000, 1_000_000_000_000]
    mags.reject { |mag| self / mag == 0 }.last
  end

end
